Beschreibung : 

    3 Nachrichten mit Daten werden von msg_mock.json gelesen und nach Server geschickt.
    Die 3 Nachrichten werden mit DevAddr, NwksKey, und AppsKey kodiert und dann in Server
    enkodiert. Die enkodierte DevAddr, AppsKey, NwksKey sowie Data werden gespeichert. 
    2 von 3 Nachrichten werden gespeichert und eine Nachricht wird rausgefiltert,
    da die DevAddr nicht richtig ist.
Test        : 

    1. LoraWanApbApplication.java wird ausgeführt.
    2. Gehe zu http://localhost:8081/adhoc-server.
    3. 2 Nachrichten mit DevAddr, AppsKey, NwksKey sowie Data werden zurückgeliefert.
    
Test UDP :

    Motes Test wird in UPDTest.java (In src/test/LoraWAN_Test/UDPTest.java) ausgeführt

Test Encryption/Decryption 

    Es wird in EncodeDecodeTest.java (In src/test/LoraWAN_Test/EncodeDecodeTest.java) ausgeführt
