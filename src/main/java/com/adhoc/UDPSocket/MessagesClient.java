package com.adhoc.UDPSocket;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import com.adhoc.lorawan.LoraWAN_Utils.JSMsgEncoderImpl;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Direction;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;

public class MessagesClient {
    private DatagramSocket socket;
    private InetAddress address;
    JSMsgEncoderImpl jsMsgEncoderImpl = new JSMsgEncoderImpl();

    private byte[] buf;

    public MessagesClient() throws SocketException, UnknownHostException {
        socket = new DatagramSocket();
        address = InetAddress.getByName("localhost");
    }

    public void sendMessage() throws Exception {
        byte[] packetMsg;
        byte[] mic;

        jsMsgEncoderImpl.encode(Msg_Direction.UPLINK, Msg_Target.hNS, Msg_Target.JS);
        packetMsg = jsMsgEncoderImpl.getPacket();
        mic = jsMsgEncoderImpl.getMic();
        String packetString = new String(packetMsg);
        String micString = new String(mic);

        String sendPacket = packetString + micString;

        buf = sendPacket.getBytes();

        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 8081);
        socket.send(packet);
    
    }

    public void sendStop(String msg) throws Exception {
        buf = msg.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 8081);
        socket.send(packet);
    }

    public void close() {
        socket.close();
    }

}