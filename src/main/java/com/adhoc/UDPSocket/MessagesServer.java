package com.adhoc.UDPSocket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import com.adhoc.lorawan.LoraWAN_Utils.NSMsgDecoderImpl;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Direction;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;

public class MessagesServer extends Thread {
    private DatagramSocket socket;
    private boolean running;
    int count = 0;
    private byte[] buf = new byte[1024];
    NSMsgDecoderImpl nsMsgDecoderImpl = new NSMsgDecoderImpl();

    public MessagesServer() throws SocketException {
        socket = new DatagramSocket(8081);
    }

    public void run() {
        running = true;

        while (count < 10) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            try {
                socket.receive(packet);
            } catch (IOException e) {
                e.printStackTrace();
            }

            InetAddress address = packet.getAddress();
            int port = packet.getPort();
            packet = new DatagramPacket(buf, buf.length, address, port);
            String received = new String(packet.getData(), 0, packet.getLength());

            String packetString = received.substring(0, 108);
            String micString = received.substring(108, 108 + 152);

            try {
                nsMsgDecoderImpl.decode(Msg_Direction.UPLINK, Msg_Target.hNS, Msg_Target.JS, packetString, micString);
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.println("DEVADDR : " + nsMsgDecoderImpl.getDevAddr());
            System.out.println("APPKEY : " + nsMsgDecoderImpl.getAppsKey());
            System.out.println("NWKKEY : " + nsMsgDecoderImpl.getNwksKey());
            System.out.println("NWKSENCKEY : " + nsMsgDecoderImpl.getNwksencKey());
            System.out.println("SNWKSINTKEY : " + nsMsgDecoderImpl.getSnwksintKey());
            System.out.println("FNWKSINTKEY : " + nsMsgDecoderImpl.getFnwksintKey());

        }
        socket.close();
    }

}