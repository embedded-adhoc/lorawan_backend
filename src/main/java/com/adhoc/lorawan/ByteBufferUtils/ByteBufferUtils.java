package com.adhoc.lorawan.ByteBufferUtils;

import java.nio.ByteBuffer;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class ByteBufferUtils {
    public static ByteBuffer HexStr2ByteBuffer(String hexStr) throws DecoderException {
        ByteBuffer wrap = ByteBuffer.wrap(Hex.decodeHex(hexStr.toCharArray()));
        return wrap;
    }

    public static String reverseString(String str) {
        StringBuilder stringBuilder = new StringBuilder(str);
        return stringBuilder.reverse().toString();
    }

    public static String reverseStringLoramotes(String str) {
        String loramotes_str;
        loramotes_str = reverseString(str);
        for (int i = 0; i < str.length(); i += 2) {
            loramotes_str = swap(loramotes_str, i, i + 1);
        }
        return loramotes_str;
    }

    private static String swap(String str, int i, int j) {
        StringBuilder sb = new StringBuilder(str);
        sb.setCharAt(i, str.charAt(j));
        sb.setCharAt(j, str.charAt(i));
        return sb.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
           data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

}