package com.adhoc.lorawan.Controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.adhoc.lorawan.Model.*;
import com.adhoc.lorawan.LoraWAN_Utils.NSMsgDecoderImpl;
import com.adhoc.lorawan.Model.MessageResponse;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Direction;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/adhoc-server")
public class UpLinkDataFromGateWay {

    Map<String, LoraSystemSimpleMsg> message;

    @GetMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
    public List<MessageResponse> getMessage() throws Exception {
        String message_raw;
        ClassLoader classLoader = getClass().getClassLoader();

        File apb_info = new File(classLoader.getResource("apb.json").getFile());
        File mock_messages = new File(classLoader.getResource("msg_mock.json").getFile());
        NSMsgDecoderImpl decoder = new NSMsgDecoderImpl();

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode apb_rootNode = objectMapper.readTree(apb_info);
        JsonNode mock_messages_rootNode = objectMapper.readTree(mock_messages);

        message_raw = mock_messages_rootNode.path("Message").path("msg_uplink").toString();

        String DEVADDR = apb_rootNode.path("devaddr").asText();
        String APPSKEY = apb_rootNode.path("appkey").asText();
        String NWKSKEY = apb_rootNode.path("nwkkey").asText();

        List<MessageResponse> messageResponses = new ArrayList<MessageResponse>();

        List<LoraSystemSimpleMsg> messages = Arrays
                .asList(objectMapper.readValue(message_raw, LoraSystemSimpleMsg[].class));
        for (LoraSystemSimpleMsg message : messages) {
            decoder.decode(Msg_Direction.valueOf(message.getDirection()), Msg_Target.valueOf(message.getSrc()),
                    Msg_Target.valueOf(message.getDst()), message.getMsg(), "hung");
            if (decoder.getDevAddr().equals(DEVADDR) && decoder.getNwksKey().equals(NWKSKEY)
                    && decoder.getAppsKey().equals(APPSKEY)) {
                messageResponses.add(new MessageResponse(message.getMsg(), message.getData(), decoder.getDevAddr(),
                        decoder.getNwksKey(), decoder.getAppsKey()));
            }
        }
        return messageResponses;
    }

    @PostMapping(produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, consumes = {
            MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<LoraSystemSimpleMsg> postMessage(@RequestBody LoraSystemSimpleMsg messageFromNode) {
        messageFromNode.setData("0x00");
        return new ResponseEntity<LoraSystemSimpleMsg>(messageFromNode, HttpStatus.ACCEPTED);
    }
}
