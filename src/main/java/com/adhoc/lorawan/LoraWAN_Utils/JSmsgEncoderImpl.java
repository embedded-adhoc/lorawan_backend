package com.adhoc.lorawan.LoraWAN_Utils;

import java.io.File;
import java.io.IOException;

import com.adhoc.lorawan.ByteBufferUtils.ByteBufferUtils;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSMsgEncoderImpl extends LoraWANMsgDecoderImpl {
    private byte[] packet;
    private byte[] mic;
    private String secret = "ad-hoc-secret";

    @Override
    protected void encode_hNS_msg(Msg_Target dest) throws Exception {
        switch (dest) {
            case hNS:
            case ED:
            case vNS:
            case sNS:
            case fNS:
            case AS:
            case JS:
                encryptionPACKET();
                encryptionMIC();
                break;
            default:
                throw new Exception("interface unimplemented or unsupported");
        }
    }

    private void encryptionPACKET() throws JsonProcessingException, IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        ObjectMapper objectMapper = new ObjectMapper();

        File apb_info = new File(classLoader.getResource("apb.json").getFile());

        JsonNode apb_rootNode = objectMapper.readTree(apb_info);

        String DEVADDR = apb_rootNode.path("devaddr").asText();
        String APPSKEY = apb_rootNode.path("appkey").asText();
        String NWKSKEY = apb_rootNode.path("nwkkey").asText();

        String encodeDEVADDR = ByteBufferUtils.reverseStringLoramotes(DEVADDR);
        String encodeNWKSKEY = ByteBufferUtils.reverseStringLoramotes(NWKSKEY);
        String encodeAPPSKEY = ByteBufferUtils.reverseStringLoramotes(APPSKEY);

        String endecoMessage = encodeDEVADDR + encodeNWKSKEY + encodeAPPSKEY;
        endecoMessage = AES.encrypt(endecoMessage, secret);
        this.packet = endecoMessage.getBytes();
    }

    private void encryptionMIC() throws JsonProcessingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ClassLoader classLoader = getClass().getClassLoader();
        File apb_info = new File(classLoader.getResource("apb.json").getFile());

        JsonNode apb_rootNode = objectMapper.readTree(apb_info);

        String NWKSENCKEY = apb_rootNode.path("nwksenckey").asText();
        String SNWKSINTKEY = apb_rootNode.path("snwksintkey").asText();
        String FNWKSINTKEY = apb_rootNode.path("fnwksintkey").asText();

        String encodeNWKSENCKEY = ByteBufferUtils.reverseStringLoramotes(NWKSENCKEY);
        String encodeSNWKSINTKEY = ByteBufferUtils.reverseStringLoramotes(SNWKSINTKEY);
        String encodeFNWKSINTKEY = ByteBufferUtils.reverseStringLoramotes(FNWKSINTKEY);

        String endecoMessage = encodeNWKSENCKEY + encodeSNWKSINTKEY + encodeFNWKSINTKEY;

        endecoMessage = AES.encrypt(endecoMessage, secret);
        this.mic = endecoMessage.getBytes();
    }

    public byte[] getPacket() throws JsonProcessingException, IOException {
        return this.packet;
    }

    public byte[] getMic() throws JsonProcessingException, IOException {
        return this.mic;
    }

}