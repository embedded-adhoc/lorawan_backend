package com.adhoc.lorawan.LoraWAN_Utils;

import com.adhoc.lorawan.MsgDecoder.MsgDecoder;
import com.adhoc.lorawan.MsgDecoder.Constants.LoraWAN_Constant;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Direction;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;

import org.apache.commons.codec.DecoderException;

public class LoraWANMsgDecoderImpl implements MsgDecoder {
    protected static LoraWAN_Constant CONSTANTS = null;

    static {
        try {
            CONSTANTS = new LoraWAN_Constant();
        } catch (DecoderException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void decode(Msg_Direction msgType, Msg_Target src, Msg_Target dest, String msg, String mic) throws Exception {
        switch (src) {
            case ED:
                decode_ED_msg(dest, msg);
                break;
            case vNS:
                decode_vNS_msg(dest, msg);
                break;
            case hNS:
                decode_hNS_msg(dest, msg, mic);
                break;
            case sNS:
                decode_sNS_msg(dest, msg);
                break;
            case fNS:
                decode_fNS_msg(dest, msg);
                break;
            case JS:
                decode_JS_msg(dest, msg);
                break;
            case AS:
                decode_AS_msg(dest, msg);
                break;
        }

    }

    @Override
    public void encode(Msg_Direction msgType, Msg_Target src, Msg_Target dest) throws Exception {
        switch (src) {
            case ED:
                encode_ED_msg(dest);
                break;
            case vNS:
                encode_vNS_msg(dest);
                break;
            case hNS:
                encode_hNS_msg(dest);
                break;
            case sNS:
                encode_sNS_msg(dest);
                break;
            case fNS:
                encode_fNS_msg(dest);
                break;
            case JS:
                encode_JS_msg(dest);
                break;
            case AS:
                encode_AS_msg(dest);
                break;
        }

    }

    protected void decode_ED_msg(Msg_Target dest, String msg) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void decode_vNS_msg(Msg_Target dest, String msg) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void decode_hNS_msg(Msg_Target dest, String msg, String mic) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void decode_sNS_msg(Msg_Target dest, String msg) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void decode_fNS_msg(Msg_Target dest, String msg) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void decode_JS_msg(Msg_Target dest, String msg) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void decode_AS_msg(Msg_Target dest, String msg) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void encode_ED_msg(Msg_Target dest) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void encode_vNS_msg(Msg_Target dest) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void encode_hNS_msg(Msg_Target dest) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void encode_sNS_msg(Msg_Target dest) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void encode_fNS_msg(Msg_Target dest) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void encode_JS_msg(Msg_Target dest) throws Exception {
        throw new Exception("not yet implemented");
    }

    protected void encode_AS_msg(Msg_Target dest) throws Exception {
        throw new Exception("not yet implemented");
    }

}