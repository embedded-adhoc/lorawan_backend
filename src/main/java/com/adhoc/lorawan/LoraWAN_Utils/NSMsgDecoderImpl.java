package com.adhoc.lorawan.LoraWAN_Utils;

import java.util.Arrays;

import com.adhoc.lorawan.ByteBufferUtils.ByteBufferUtils;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;

import org.apache.commons.codec.binary.Hex;

public class NSMsgDecoderImpl extends LoraWANMsgDecoderImpl {
    private final String secret = "ad-hoc-secret";

    static final int DEVADDR_OFFSET = 0;
    static final int DEVADDR_LEN = DEVADDR_OFFSET + 4;

    static final int NWKSKEY_OFFSET = DEVADDR_LEN;
    static final int NWKSKEY_LEN = NWKSKEY_OFFSET + 16;

    static final int APPSKEY_OFFSET = NWKSKEY_LEN;
    static final int APPSKEY_LEN = APPSKEY_OFFSET + 16;

    static final int NWKSENCKEY_OFFSET = 0;
    static final int NWKSENCKEY_LEN = NWKSENCKEY_OFFSET + 16;

    static final int SNWKSINTKEY_OFFSET = NWKSENCKEY_LEN;
    static final int SNWKSINTKEY_LEN = SNWKSINTKEY_OFFSET + 16;

    static final int FNWKSINTKEY_OFFSET = SNWKSINTKEY_LEN;
    static final int FNWKSINTKEY_LEN = FNWKSINTKEY_OFFSET + 16;

    private String devAddr;
    private String nwksKey;
    private String appsKey;

    private String nwksencKey;
    private String snwksintKey;
    private String fnwksintKey;

    byte[] Packet;
    byte[] Mic;

    @Override
    protected void decode_hNS_msg(Msg_Target dest, String msg, String mic) throws Exception {
        msg = AES.decrypt(msg, secret);
        mic = AES.decrypt(mic, secret);

        Packet = ByteBufferUtils.HexStr2ByteBuffer(msg).array();
        Mic = ByteBufferUtils.HexStr2ByteBuffer(mic).array();
        switch (dest) {
            case hNS:
            case ED:
            case vNS:
            case sNS:
            case fNS:
            case AS:
            case JS:
                decodeDEVADDR();
                decodeNWKSKEY();
                decodeAPPSKEY();
                decodeFNWKSINTKEY();
                decodeNWKSENCKEY();
                decodeSNWKSINTKEY();
                break;
            default:
                throw new Exception("interface unimplemented or unsupported");
        }
    }

    private void decodeNWKSENCKEY() {
        byte[] nwksenckey;
        nwksenckey = Arrays.copyOfRange(Mic, NWKSENCKEY_OFFSET, NWKSENCKEY_LEN);
        this.nwksencKey = ByteBufferUtils.reverseStringLoramotes(Hex.encodeHexString(nwksenckey));

    }

    private void decodeSNWKSINTKEY() {
        byte[] snwksintkey;
        snwksintkey = Arrays.copyOfRange(Mic, SNWKSINTKEY_OFFSET, SNWKSINTKEY_LEN);
        this.snwksintKey = ByteBufferUtils.reverseStringLoramotes(Hex.encodeHexString(snwksintkey));
    }

    private void decodeFNWKSINTKEY() {
        byte[] fnwksintkey;
        fnwksintkey = Arrays.copyOfRange(Mic, FNWKSINTKEY_OFFSET, FNWKSINTKEY_LEN);
        this.fnwksintKey = ByteBufferUtils.reverseStringLoramotes(Hex.encodeHexString(fnwksintkey));
    }

    private void decodeDEVADDR() {
        byte[] devaddr;
        devaddr = Arrays.copyOfRange(Packet, DEVADDR_OFFSET, DEVADDR_LEN);
        devAddr = ByteBufferUtils.reverseStringLoramotes(Hex.encodeHexString(devaddr));
    }

    private void decodeNWKSKEY() {
        byte[] nwkskey;
        nwkskey = Arrays.copyOfRange(Packet, NWKSKEY_OFFSET, NWKSKEY_LEN);
        this.nwksKey = ByteBufferUtils.reverseStringLoramotes(Hex.encodeHexString(nwkskey));
    }

    private void decodeAPPSKEY() {
        byte[] appskey;
        appskey = Arrays.copyOfRange(Packet, APPSKEY_OFFSET, APPSKEY_LEN);
        this.appsKey = ByteBufferUtils.reverseStringLoramotes(Hex.encodeHexString(appskey));
    }

    public String getNwksencKey() {
        return this.nwksencKey;
    }

    public String getSnwksintKey() {
        return this.snwksintKey;
    }

    public String getFnwksintKey() {
        return this.fnwksintKey;
    }

    public String getDevAddr() {
        return this.devAddr;
    }

    public String getNwksKey() {
        return this.nwksKey;
    }

    public String getAppsKey() {
        return this.appsKey;
    }

}