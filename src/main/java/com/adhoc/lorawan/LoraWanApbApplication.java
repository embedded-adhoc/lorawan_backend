package com.adhoc.lorawan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoraWanApbApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoraWanApbApplication.class, args);
	}

}
