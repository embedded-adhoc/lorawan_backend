package com.adhoc.lorawan.Model;

public class LoraSystemSimpleMsg {
    private String src;
    private String dst;
    private String msg;
    private String direction;
    private String data;

    public LoraSystemSimpleMsg(String src, String dst, String msg, String direction, String data) {
        this.src = src;
        this.dst = dst;
        this.msg = msg;
        this.direction = direction;
        this.data = data;
    }

    public LoraSystemSimpleMsg() {
        super();
    }

    public String getSrc() {
        return this.src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getDst() {
        return this.dst;
    }

    public void setDst(String dst) {
        this.dst = dst;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDirection() {
        return this.direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

}