package com.adhoc.lorawan.Model;

public class MessageResponse {
    private String msg;
    private String data;

    private String devAddr;
    private String nwksKey;
    private String appsKey;

    public MessageResponse(String msg, String data, String devAddr, String nkwsKey, String appsKey) {
        this.msg = msg;
        this.data = data;
        this.devAddr = devAddr;
        this.nwksKey = nkwsKey;
        this.appsKey = appsKey;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDevAddr() {
        return this.devAddr;
    }

    public void setDevAddr(String devAddr) {
        this.devAddr = devAddr;
    }

    public String getNwksKey() {
        return this.nwksKey;
    }

    public void setNwksKey(String nwksKey) {
        this.nwksKey = nwksKey;
    }

    public String getAppsKey() {
        return this.appsKey;
    }

    public void setAppsKey(String appsKey) {
        this.appsKey = appsKey;
    }

}