package com.adhoc.lorawan.MsgDecoder.Enums;

public enum Msg_Direction {
    UPLINK,
    DOWNLINK
}
