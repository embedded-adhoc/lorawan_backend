package com.adhoc.lorawan.MsgDecoder.Enums;

/**
 * ED - End device
 * vNS - additional Network Server for Roaming Activation
 * hNS - home net work server
 * sNS - serving network server
 * fNS - forwarding network server
 * JS - join server
 * AS - application server
 */
public enum Msg_Target {
    ED,
    vNS,
    hNS,
    sNS,
    fNS,
    JS,
    AS,
}
