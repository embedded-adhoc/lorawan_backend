package com.adhoc.lorawan.MsgDecoder;

import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Direction;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;

public interface MsgDecoder {
    void decode(Msg_Direction msgType, Msg_Target src, Msg_Target dest, String msg, String mic) throws Exception;

    void encode(Msg_Direction msgType, Msg_Target src, Msg_Target dest) throws Exception;

}
