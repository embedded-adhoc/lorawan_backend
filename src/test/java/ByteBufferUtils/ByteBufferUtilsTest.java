package ByteBufferUtils;

import com.adhoc.lorawan.ByteBufferUtils.ByteBufferUtils;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class ByteBufferUtilsTest {
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    @Test
    public void hexStr2ByteBuffer() throws DecoderException {
        String test_str = "0123456789abcdef";
        byte test_str_bytes_array[] = {
                0x01, 0x23, 0x45, 0x67, (byte) 0x89, (byte) 0xab, (byte) 0xcd, (byte) 0xef
        };
        String test_str2 = "00123456789abcdef0";
        byte test_str_bytes_array2[] = {
                0x00, 0x12, 0x34, 0x56, 0x78, (byte) 0x9a, (byte) 0xbc, (byte) 0xde, (byte) 0xf0
        };
        String test_str3 = "0f123456789abcdef0";
        byte test_str_bytes_array3[] = {
                0x0f, 0x12, 0x34, 0x56, 0x78, (byte) 0x9a, (byte) 0xbc, (byte) 0xde, (byte) 0xf0
        };
        System.out.println(Hex.encodeHexString(ByteBufferUtils.HexStr2ByteBuffer(test_str).array()));
        assertArrayEquals(test_str_bytes_array, ByteBufferUtils.HexStr2ByteBuffer(test_str).array());
        System.out.println(Hex.encodeHexString(ByteBufferUtils.HexStr2ByteBuffer(test_str2).array()));
        assertArrayEquals(test_str_bytes_array2, ByteBufferUtils.HexStr2ByteBuffer(test_str2).array());
        System.out.println(Hex.encodeHexString(ByteBufferUtils.HexStr2ByteBuffer(test_str3).array()));
        assertArrayEquals(test_str_bytes_array3, ByteBufferUtils.HexStr2ByteBuffer(test_str3).array());
    }

    @Test
    public void reverseString() {
        String test_str = "0123456789abcdef";
        String reverse_test_str = "fedcba9876543210";
        assertEquals(ByteBufferUtils.reverseString(test_str),reverse_test_str);
    }

    @Test
    public void reverseStringLoramotes() {
        String test_str = "0123456789abcdef";
        String reverse_test_str = "efcdab8967452301";
        assertEquals(ByteBufferUtils.reverseStringLoramotes(test_str),reverse_test_str);
    }
}