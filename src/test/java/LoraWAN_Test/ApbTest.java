package LoraWAN_Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import com.adhoc.lorawan.Model.*;
import com.adhoc.lorawan.ByteBufferUtils.ByteBufferUtils;
import com.adhoc.lorawan.LoraWAN_Utils.JSMsgEncoderImpl;
import com.adhoc.lorawan.LoraWAN_Utils.NSMsgDecoderImpl;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Direction;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;


class ApbTest {
    @Test
    public void decode_hNS_msg() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File apb_info = new File(classLoader.getResource("apb.json").getFile());
        File mock_messages = new File(classLoader.getResource("msg_mock.json").getFile());

        String testDEVADDR;
        String testNWKSKEY;
        String testAPPSKEY;

        String message_raw;

        NSMsgDecoderImpl Decoder = new NSMsgDecoderImpl();

        ObjectMapper objectMapper = new ObjectMapper();

        JsonNode apb_rootNode = objectMapper.readTree(apb_info);
        JsonNode mock_messages_rootNode = objectMapper.readTree(mock_messages);

        testDEVADDR = apb_rootNode.path("devaddr").asText();
        testAPPSKEY = apb_rootNode.path("appkey").asText();
        testNWKSKEY = apb_rootNode.path("nwkkey").asText();
        
        String encodeDEVADDR = ByteBufferUtils.reverseStringLoramotes(testDEVADDR);
        String encodeNWKSKEY = ByteBufferUtils.reverseStringLoramotes(testNWKSKEY);
        String encodeAPPSKEY = ByteBufferUtils.reverseStringLoramotes(testAPPSKEY);

        String encodeMessage = encodeDEVADDR + encodeNWKSKEY + encodeAPPSKEY;

        message_raw = mock_messages_rootNode.path("Message").path("msg_uplink").toString();

        byte[] packet;
        byte[] mic;

        JSMsgEncoderImpl jSmsgEncoderImpl = new JSMsgEncoderImpl();
        jSmsgEncoderImpl.encode(Msg_Direction.UPLINK, Msg_Target.hNS, Msg_Target.JS);
        packet = jSmsgEncoderImpl.getPacket();
        mic = jSmsgEncoderImpl.getMic();

        String packetString = new String(packet);
        String micString = new String(mic);

    
        List<LoraSystemSimpleMsg> messages = Arrays
                .asList(objectMapper.readValue(message_raw, LoraSystemSimpleMsg[].class));
        ;
        for (LoraSystemSimpleMsg message : messages) {
            Decoder.decode(Msg_Direction.valueOf(message.getDirection()), Msg_Target.valueOf(message.getSrc()),
                    Msg_Target.valueOf(message.getDst()), packetString, micString);
            assertEquals(testDEVADDR.toLowerCase(), Decoder.getDevAddr().toLowerCase());
            assertEquals(testNWKSKEY.toLowerCase(), Decoder.getNwksKey().toLowerCase());
            assertEquals(testAPPSKEY.toLowerCase(), Decoder.getAppsKey().toLowerCase());

        }

    }

}
