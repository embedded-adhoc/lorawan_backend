package LoraWAN_Test;

import com.adhoc.lorawan.LoraWAN_Utils.JSMsgEncoderImpl;
import com.adhoc.lorawan.LoraWAN_Utils.NSMsgDecoderImpl;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Direction;
import com.adhoc.lorawan.MsgDecoder.Enums.Msg_Target;

import org.junit.Test;

public class EncodeDecodeTest {

    JSMsgEncoderImpl jsMsgEncoderImpl = new JSMsgEncoderImpl();
    NSMsgDecoderImpl nsMsgDecoderImpl = new NSMsgDecoderImpl();

    @Test
    public void encodeDecode() throws Exception {
        byte[] packet;
        byte[] mic;

        jsMsgEncoderImpl.encode(Msg_Direction.UPLINK, Msg_Target.hNS, Msg_Target.JS);
        packet = jsMsgEncoderImpl.getPacket();
        mic = jsMsgEncoderImpl.getMic();

        String packetString = new String(packet);
        String micString = new String(mic);

        nsMsgDecoderImpl.decode(Msg_Direction.UPLINK, Msg_Target.hNS, Msg_Target.JS, packetString, micString);

        System.out.println("DEVADDR : " + nsMsgDecoderImpl.getDevAddr());
        System.out.println("APPKEY : " + nsMsgDecoderImpl.getAppsKey());
        System.out.println("NWKKEY : " + nsMsgDecoderImpl.getNwksKey());
        System.out.println("NWKSENCKEY : " + nsMsgDecoderImpl.getNwksencKey());
        System.out.println("SNWKSINTKEY : " + nsMsgDecoderImpl.getSnwksintKey());
        System.out.println("FNWKSINTKEY : " + nsMsgDecoderImpl.getFnwksintKey());

    }

}