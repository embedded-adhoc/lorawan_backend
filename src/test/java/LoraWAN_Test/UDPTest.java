package LoraWAN_Test;

import org.junit.Before;
import org.junit.Test;

import java.net.SocketException;
import java.net.UnknownHostException;

import com.adhoc.UDPSocket.MessagesClient;
import com.adhoc.UDPSocket.MessagesServer;

public class UDPTest {
    MessagesClient client;
    MessagesServer server = new MessagesServer();

    public UDPTest() throws SocketException {
    }

    @Before
    public void setup() throws SocketException, UnknownHostException {
        server.start();
        client = new MessagesClient();
    }

    @Test
        public void TestUDP() throws Exception {
        client.sendMessage();
        client.close();
    }

}